/**
 * Created by Jógvan on 17/03-2016 13:08.
 */

var express = require('express');
var app = express();
var fs = require('fs');

app.use(express.static('./public'));

var about = {
    title: "Wait",
    subtitle: "Not loaded",
    text: "Content is not loaded yet, please come by later"
};

fs.readFile("about.txt", 'utf8', function(err, data){
    var ar = data.split("\n");
    about.title = ar[0];
    about.subtitle = ar[1];
    about.text = ar[2];
});

app.set('view engine', 'ejs');
app.get('/', function (req, res) {
    res.render("page", {
        title: "Home",
        subtitle: "Homepage for the Mandatory Assignment",
        text: "This is some text I choose to put in this paragraph"
    });
});

app.get('/about', function(req, res){
    res.render("page", about);
});

app.listen(3000);